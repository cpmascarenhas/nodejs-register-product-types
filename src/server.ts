import App from './app'


if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

App.express.listen(process.env.PORT || 3002, () => {
    console.log("rodando na porta 3002")
})