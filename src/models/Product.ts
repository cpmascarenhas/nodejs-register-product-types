import mongoose, { Schema, model, Document } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate'



interface ProductInterface extends Document {
    titulo?: string;
    descricao?: string
    url?: string
}

const ProductSchema = new Schema({

    titulo: String,
    descricao: String,
    url: String,
})


export default model<ProductInterface>('Product', ProductSchema)

// mongoose.model('/Product', ProductSchema)
// ProductSchema.plugin(mongoosePaginate);
