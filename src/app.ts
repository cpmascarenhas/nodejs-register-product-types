import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import * as dotenv from "dotenv";
import routes from './routes'

dotenv.config();

class App {


    public express: express.Application

    public constructor() {
        this.express = express()
        this.middlewares();
        this.database();
        this.routes();
    }

    private middlewares(): void {
        this.express.use(express.json())
    }

    private database(): void {
        mongoose.connect(`${process.env.DATABASE_CONNECTION_STRING}`, {
            useUnifiedTopology: true,
            useFindAndModify: true,
            useNewUrlParser: true,
            useCreateIndex: true,
        }, (err) => {
            if (err) {
                console.log(err)
                throw new Error('CONNECTION FAILED')
            }
            console.log('Conectado')
        })
    }

    private routes(): void {
        this.express.use(routes)
    }

}

export default new App;