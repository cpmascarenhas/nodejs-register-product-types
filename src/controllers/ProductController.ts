import mongoose from 'mongoose';
import Product from '../models/Product'
import { Request, Response } from 'express'


class ProductController {

    public async index(req: Request, res: Response): Promise<Response> {
        const products = await Product.find()
        return res.json(products)
    }

    public async show(req: Request, res: Response): Promise<Response> {
        const products = await Product.findById(req.params.id)
        return res.json(products)
    }

    public async update(req: Request, res: Response): Promise<Response> {
        const products = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true })
        return res.json(products)
    }

    public async create(req: Request, res: Response): Promise<Response> {
        const product = await Product.create(req.body)
        return res.json(product)
    }
    public async destroy(req: Request, res: Response): Promise<Response> {
        const product = await Product.findByIdAndDelete(req.params.id)
        return res.json(product)
    }
}

export default new ProductController()



