Para criar um projeto Node com TypeScript utilizei os seguintes comando.

npm i -D ts-node-dev typescript: Dependencias de desenvolvimento.
npx tsc --init: Criar arquivo tsconfig.json

Recursos para aplicativos WEB:
npm i express
npm i -D @types/express

package.json:
"start": "ts-node-dev --transpileOnly --ignore-watch node_modules index.ts"

--transpileOnly vai dizer para ele apenas transpilar, sem fazer verificação de tipos. Esta verificação pode ser feita direto no editor, de forma muito mais ágil, apontando exatamente no seu código onde estão os erros.

--ignore-watch node_modules está dizendo para ignorar arquivos do node_modules, já que não vamos mexer nestes arquivos e esperamos que eles já estejam em JavaScript.